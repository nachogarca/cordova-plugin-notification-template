// Empty constructor
function NotifyTemplate() {}

// The function that passes work along to native shells
// Message is a string, duration may be 'long' or 'short'
NotifyTemplate.prototype.start = function(url_ws, successCallback, errorCallback) {
  var options = {};
  options.url = url_ws;
  cordova.exec(successCallback, errorCallback, 'ServicePlugin', 'start', [options]);
};

NotifyTemplate.prototype.isOpen = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'ServicePlugin', 'isOpen', [options]);
};

NotifyTemplate.prototype.shutdown = function(successCallback, errorCallback) {
  var options = {};
  cordova.exec(successCallback, errorCallback, 'ServicePlugin', 'shutdown', [options]);
};

if (!android){
    function android(){};
}

// Installation constructor that binds ToastyPlugin to window
NotifyTemplate.install = function() {

  if (!window.plugins) {
    window.plugins = {};
  }
  window.plugins.notify = new NotifyTemplate();
  return window.plugins.notify;
};
cordova.addConstructor(NotifyTemplate.install);
