package com.nachogarca.cordova.plugin;

import android.content.Context;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;

public class Shared{
    
    public static Thread WorkerThread = null;
    public static int NotificationID=7824;
    private static final String NombreCache = "cordova-plugin-notification-template-shared";
    public static WebsocketClient WebSocketClient = null;
    
    public static void set(Context context, String key, String value){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
        
    }
    
    public static String get(Context context, String key){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        String value = sharedPref.getString(key, "");        
        return value;
    }
    
}