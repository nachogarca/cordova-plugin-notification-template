package com.nachogarca.cordova.plugin;

import android.app.Service;
import android.os.Binder;

public class ServiceBinder extends Binder {
	public final Service service;

	public ServiceBinder(Service service) {
		this.service = service;
	}
}