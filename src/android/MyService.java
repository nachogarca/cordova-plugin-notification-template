package com.nachogarca.cordova.plugin;

import java.net.URI;
import android.app.Service;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

import android.util.Log;
import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.os.Build;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.net.Uri;
import android.app.NotificationChannel;


public class MyService extends Service {
    private final IBinder mBinder = new ServiceBinder(this);
    private static String debug = "";
    public static Boolean AutoClose = false;

    public MyService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        showNotification("oncreate", "");
        Log.i(getClass().getSimpleName(), "Creating service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        
        showNotification("debug-1","autoClose:" + MyService.AutoClose);        
        
        
        Boolean condicion = Shared.WebSocketClient  == null;
        if (condicion == false){
            condicion = Shared.WebSocketClient.getSession() == null;
            if (condicion == false){
                condicion = !Shared.WebSocketClient.getSession().isOpen();
            }
        }
        
        if(MyService.AutoClose){
            if (condicion == false){
                try{
                    Shared.WebSocketClient.getSession().close();
                }catch(Exception e){}
            }
            stopSelf();
            return START_STICKY;
            
        }        
        
        showNotification("debug0","");

        Context context = getApplicationContext();
        String url = Shared.get(context,"url");
        debug = " D0";  
        
        //evitamos que el hilo se ejecute dos veces al mismo tiempo
        Boolean condicion2 = Shared.WorkerThread == null;
        condicion2 = condicion2 == false ? !Shared.WorkerThread.isAlive() : true;
        
        if(condicion == true && condicion2 == true){
            
            Shared.WorkerThread = new Thread(new Runnable() {
                public void run() {   
                    while(true){           
                        try{
                            showNotification("debug1","Intento de conexion");
                            Shared.WebSocketClient = new WebsocketClient(new URI(url));
                            showNotification("debug2","conexion establecida");
                            Shared.WebSocketClient.addHandler(
                                    new WebsocketClient.MessageHandler() {
                                        public void handleMessage(String message) {
                                            showNotification("titulo", message);
                                            System.out.println(message);
                                        }
                                    }
                            );
                            showNotification("debug3","");
                            Shared.WebSocketClient.sendMessage("mensaje demostracion");
                            showNotification("debug4","mensaje enviado");                            
                            break;
                        }catch(Exception e){
                            showNotification("error", "["+debug+"] "+e.getMessage());
                        }
                        
                        try{
                            Thread.sleep(5000); 
                        }catch(Exception e){}
                    //clientEndPoint.sendMessage("{'event':'addChannel','channel':'ok_btccny_ticker'}");
                    }
                }
            });
            Shared.WorkerThread.start();
           
        }else{
            showNotification("ya en ejecucion","");
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {        
        super.onDestroy();
        showNotification("destroy", "");
    }

    
    @Override
    public IBinder onBind(Intent intent) {
            showNotification("onBind", "");
            Context context = getApplicationContext();
            Shared.set(context,"url",intent.getStringExtra("urlws"));   
            return mBinder;
    }
    
    
    //NOTIFICATION:
    private void showNotification(String titulo, String contenido) {
        Notification notification = createBuilder(titulo,contenido).build();        
        getNotificationManager().notify(Shared.NotificationID++, notification);
    }
    
    private NotificationManager getNotificationManager() {
        //final Activity activity = this.cordova.getActivity();
        return (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }
    
    private Notification.Builder createBuilder(String titulo, String contenido){
            Context context = getApplicationContext();
            String pkgName  = context.getPackageName();
            Intent resultIntent   = context.getPackageManager().getLaunchIntentForPackage(pkgName);
        
            //Context context = ServicePlugin.Activity;
            Notification.Builder builder = new Notification.Builder(context);

            //Configure builder
            builder.setContentTitle(titulo+"["+Shared.NotificationID+"]");
            builder.setContentText(contenido);
            builder.setSmallIcon(android.R.drawable.ic_menu_recent_history);
            //builder.setWhen(0);
            //builder.setOngoing(true);
            builder.setPriority(Notification.PRIORITY_MAX);
            
            //Intent resultIntent = new Intent(context, ServicePlugin.Activity.getClass());
            resultIntent.setAction(Intent.ACTION_MAIN);
            resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, 0);
            builder.setContentIntent(resultPendingIntent);
            builder.setAutoCancel(true);

            return builder;
    }



}

