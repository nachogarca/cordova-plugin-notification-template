package com.nachogarca.cordova.plugin;

//imports cordova

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;

import android.util.Log;
import android.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.os.Build;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.net.Uri;
import android.app.NotificationChannel;
import android.content.ServiceConnection;
import android.content.ComponentName;
import android.app.Service;
import android.os.IBinder;


import org.apache.cordova.CordovaInterface;

//https://github.com/homerours/cordova-music-controls-plugin

public class ServicePlugin extends CordovaPlugin {
    
    private final int notificationID=7824;
    public static Activity Activity = null;
    private static Intent startServiceIntent = null;
    
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
            super.initialize(cordova, webView);
    }      

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext){
        // Verify that the user sent a 'show' action
        /*if (!action.equals("start")) {
            callbackContext.error("\"" + action + "\" is not a recognized action.");
            return false;
        }*/
        PluginResult pluginResult = null;
        final Activity activity = this.cordova.getActivity();
        ServicePlugin.Activity = activity;
        ServiceConnection mConnection;
        
        try{
            if (action.equals("start")) {
                String url = "";
                try{
                    JSONObject options = args.getJSONObject(0);
                    url = options.getString("url");

                }catch(Exception e){
                    callbackContext.error("Error encountered: " + e.getMessage());
                    return false;
                }            

                        mConnection = new ServiceConnection() {
                                public void onServiceConnected(ComponentName className, IBinder binder) {
                                        ((ServiceBinder) binder).service.startService(new Intent(activity, MyService.class));
                                }
                                public void onServiceDisconnected(ComponentName className) {
                                }
                        };
                        startServiceIntent = new Intent(activity,MyService.class);
                        startServiceIntent.putExtra("urlws",url);
                        MyService.AutoClose = false;
                        activity.bindService(startServiceIntent, mConnection, Context.BIND_AUTO_CREATE);  

                pluginResult = new PluginResult(PluginResult.Status.OK);
            }
        }catch(Exception e){
            callbackContext.error("Error encountered: " + e.getMessage());
            return false;
        }
        
        try{
            if (action.equals("isOpen")) {             
                int result = 0;
                if (Shared.WebSocketClient != null){
                    if (Shared.WebSocketClient.getSession() != null){
                        if (Shared.WebSocketClient.getSession().isOpen()){
                            result = 1;
                        }                
                    }
                }
                pluginResult = new PluginResult(PluginResult.Status.OK, result);
            }
        }catch(Exception e){
            callbackContext.error("Error encountered: " + e.getMessage());
            return false;
        }
        
        try{
            if (action.equals("shutdown")){
                mConnection = new ServiceConnection() {
                        public void onServiceConnected(ComponentName className, IBinder binder) {
                                ((ServiceBinder) binder).service.startService(new Intent(activity, MyService.class));
                        }
                        public void onServiceDisconnected(ComponentName className) {
                        }
                };

                startServiceIntent = new Intent(activity,MyService.class);
                startServiceIntent.putExtra("urlws","");
                MyService.AutoClose = true;
                activity.bindService(startServiceIntent, mConnection, Context.BIND_AUTO_CREATE);  

                pluginResult = new PluginResult(PluginResult.Status.OK);
            }
        }catch(Exception e){
            callbackContext.error("Error encountered: " + e.getMessage());
            return false;
        }
        
        
        callbackContext.sendPluginResult(pluginResult);
        return true;
    }


}

